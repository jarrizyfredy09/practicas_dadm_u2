package com.example.practicas_u2.practicaintegradora

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.item_climas.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ClimaAdapter(private val listener: (Climas.ClimaTiempo) -> Unit) :
    RecyclerView.Adapter<ClimaViewHolder>() {

    private val List = mutableListOf<Climas.ClimaTiempo>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClimaViewHolder {
        val itemview =
            LayoutInflater.from(parent.context).inflate(R.layout.item_climas, parent, false)
        return ClimaViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: ClimaViewHolder, position: Int) {
        holder.setData(List[position], listener)
    }

    override fun getItemCount(): Int {
        return List.size
    }

    fun setList(list: ArrayList<Climas.ClimaTiempo>) {
        this.List.addAll(list)
        notifyDataSetChanged()
    }

}

class ClimaViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

    fun setData(item: Climas.ClimaTiempo, listener: (Climas.ClimaTiempo) -> Unit) {

        itemView.apply {
            val dia = getDayOfTheWeek(Date(item.time.times(1000)))
            tvDia.text = ""+dia
            imgDia.setImageResource(getWeatherIcon(item.icon))
            tvMax.text = "${item.temperatureMax}"
            tvMin.text = "${item.temperatureMin}"
            setOnClickListener { listener.invoke(item) }

        }
    }
}
fun getDayOfTheWeek(date: Date): String{
    val format = SimpleDateFormat("EEEE, d", Locale.getDefault())
    return format.format(date)
}
    fun getWeatherIcon (name: String): Int{
        return when (name){
            "clear-day" -> R.drawable.wic_clear_day
            "clear-night" -> R.drawable.wic_clear_night
            "rain" -> R.drawable.wic_rain
            "snow" -> R.drawable.wic_snow
            "wind" -> R.drawable.wic_wind
            "fog" -> R.drawable.wic_fog
            "sleet" -> R.drawable.wic_sleet
            "cloudy" -> R.drawable.wic_cloudy
            "cloudy-cloudy-day" -> R.drawable.wic_partly_cloudy_day
            "partly-cloudy-night" -> R.drawable.wic_partly_cloudy_night
            "hail" -> R.drawable.wic_hail
            "thunderstorm" -> R.drawable.wic_thunderstorm
            "tornado" -> R.drawable.wic_tornado
            else -> R.drawable.ic_error
        }
    }
