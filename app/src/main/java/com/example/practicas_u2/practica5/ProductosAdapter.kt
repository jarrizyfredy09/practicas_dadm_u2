package com.example.practicas_u2.practica5


import android.graphics.drawable.AnimatedImageDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practicas_u2.R
import com.example.practicas_u2.practica2.DurationAdapterViewHolder

class ProductosAdapter (private val listener: (Int) -> Unit) :
    RecyclerView.Adapter<DurationAdapterViewHolder>() {

    private var List = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DurationAdapterViewHolder {
        val itemview =
            LayoutInflater.from(parent.context).inflate(R.layout.item_productos, parent, false)
        return DurationAdapterViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: DurationAdapterViewHolder, position: Int) {
        holder.setData(List[position], listener)
    }

    override fun getItemCount(): Int {
        return List.size
    }

    fun setList (list: List<Int>) {
        this.List.addAll(list)
        notifyDataSetChanged()
    }

}

data class Menu(var imageDrawable: AnimatedImageDrawable, var name: String)


