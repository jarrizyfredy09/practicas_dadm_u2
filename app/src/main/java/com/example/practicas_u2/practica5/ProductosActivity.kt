package com.example.practicas_u2.practica5

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_u2.R
import com.example.practicas_u2.practica2.DurationAdapter
import kotlinx.android.synthetic.main.activity_duration_selector.*
import kotlinx.android.synthetic.main.activity_productos.*

class ProductosActivity : AppCompatActivity() {

    var durationList = mutableListOf<Int>()

    private val adapter by lazy {
        DurationAdapter{ duration ->
            intent.putExtra( "DURATION", duration)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_productos)
        durationList = mutableListOf()
        rvProductos.adapter = adapter
        adapter.setList(durationList)

    }

}

