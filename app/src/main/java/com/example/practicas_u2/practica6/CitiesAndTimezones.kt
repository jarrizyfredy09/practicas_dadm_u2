package com.example.practicas_u2.practica6

import org.json.JSONArray
import org.json.JSONObject

class CitiesAndTimezones {

    var cities = arrayListOf<CitiesTimezones>()

    constructor()

    constructor(jsonArray: JSONArray){
        for (i in 0 until jsonArray.length()){
            val data = jsonArray.getJSONObject(i)
            cities.add(CitiesTimezones(data))
        }
    }

    class CitiesTimezones(jsonObject: JSONObject){
        var name: String = ""
        var country: String = ""
        var timeZoneName: String = ""

        init {
            name = jsonObject.getString("name")
            country = jsonObject.getString("country")
            timeZoneName = jsonObject.getString("timeZoneName")
        }

    }
}