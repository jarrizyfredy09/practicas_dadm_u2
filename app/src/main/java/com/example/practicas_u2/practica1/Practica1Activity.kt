package com.example.practicas_u2.practica1

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.telephony.SmsManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.activity_practica1.*


class Practica1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1)

        btnCall.setOnClickListener {
            val telephone = etPhone.text.toString()
            if (switchCM.isChecked) {
                if (validateMessagePermission()) {
                    val mensaje = etMsj.text.toString()
                    val sms: SmsManager = SmsManager.getDefault()

                    sms.sendTextMessage(telephone, null, mensaje, null, null)

                    Toast.makeText(this, "Sent.", Toast.LENGTH_SHORT).show()
                } else {
                    val permission = arrayOf(Manifest.permission.SEND_SMS)
                    ActivityCompat.requestPermissions(this, permission, 102)
                }
            } else {
                if (validateCallPermission()) {
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$telephone"))
                    startActivity(intent)
                } else {
                    val permission = arrayOf(Manifest.permission.CALL_PHONE)
                    ActivityCompat.requestPermissions(this, permission, 101)
                }
            }
        }

        switchCM.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                switchCM.text = "Mensaje"
                etMsj.visibility = View.VISIBLE
                btnCall.text = "Enviar"
            } else {
                switchCM.text = "Llamada"
                etMsj.visibility = View.GONE
                btnCall.text = "Llamar"
            }
        }
    }

    private fun validateCallPermission(): Boolean {

        return ActivityCompat.checkSelfPermission(this,
            Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
    }

    private fun validateMessagePermission(): Boolean {

        return ActivityCompat.checkSelfPermission(this,
            Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
    }
}