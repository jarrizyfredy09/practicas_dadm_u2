package com.example.practicas_u2.practicaintegradora

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.item_climas.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter


class PracticaIntegradora : AppCompatActivity() {
    private val adapter by lazy {
        ClimaAdapter{ selectedClima ->

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.item_climas)
        rvClm.adapter = adapter

        val input = resources.openRawResource(R.raw.weather)
        val writer = StringWriter()
        val buffer = CharArray(1024)
        input.use { input ->
            val reader = BufferedReader(InputStreamReader(input, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1){
                writer.write(buffer, 0, n)
            }
        }
        val jsonObject = JSONObject(writer.toString())
        val tiempoList = Climas(jsonObject)

        adapter.setList(tiempoList.tiempo)

    }
}