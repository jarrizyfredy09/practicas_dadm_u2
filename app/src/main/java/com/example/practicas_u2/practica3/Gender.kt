package com.example.practicas_u2.practica3

enum class Gender (var namee: String){
    MALE("Hombre"),
    FEMALE("Mujer"),
    NOT_BINARY("No binario"),
    NOT_IDENTYFIED("Sin especificar")
}