package com.example.practicas_u2.practica2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.activity_practica2.*

const val DURATION_RESULT = 4000

class Practica2Activity : AppCompatActivity() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                DURATION_RESULT -> {
                    if (data != null) {
                        val minutes = data.getIntExtra("DURATION", -1)
                        if (minutes >= 60) {
                            tvSelected.text = resources.getQuantityString(R.plurals.pluralsHours, minutes / 60, minutes / 60)
                        } else {
                            tvSelected.text = "$minutes minutes"
                        }

                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica2)

        btnSelector.setOnClickListener {
            val intent = Intent(this, DurationSelectorActivity::class.java)
            startActivityForResult(intent, DURATION_RESULT)

        }


    }


}