package com.example.practicas_u2.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practicas_u2.R
import kotlinx.android.synthetic.main.activity_practica2.*
import kotlinx.android.synthetic.main.activity_practica5.*

const val DURATION_RESULT = 3000

class Practica5Activity : AppCompatActivity() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                DURATION_RESULT -> {
                    if (data != null) {
                        val minutes = data.getIntExtra("DURATION", -1)
                        if (minutes >= 60) {
                            tvSelected.text = resources.getQuantityString(R.plurals.pluralsHours, minutes / 60, minutes / 60)
                        } else {
                            tvSelected.text = "$minutes minutes"
                        }

                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_productos)

        btnMostrar.setOnClickListener {
            val intent = Intent(this, ProductosActivity::class.java)
            startActivityForResult(intent, DURATION_RESULT)

        }
    }
}